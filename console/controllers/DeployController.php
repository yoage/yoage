<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;

/**
 * init the db structure and initial data and etc.
 * this controller can do anything for you.
 *
 *Usage:
 * 1. init the db structure using the 'deploy/db-structure' command:
 *    yii deploy/db-structure
 * 2. init the initial data using the 'deploy/initial-data' command:
 *    yii deploy/initial-data
 *
 * Edit the actions , adjusting it for your needs.
 * please make sure the action is correct.
 * @package console\controllers
 */
class DeployController extends  \yii\console\Controller
{
    public function actionDbStructure()
    {

        $filePatch = Yii::getAlias("@console/sql/Step1_BasicStructure.sql");
        $sql = file_get_contents($filePatch);
        $connection = \Yii::$app->db;
        $connection->createCommand($sql)->execute();
        //return 1;
    }

    public function actionInitialData()
    {
        $filePatch = Yii::getAlias("@console/sql/Step2_InitialData.sql");
        $sql = file_get_contents($filePatch);
        $connection = \Yii::$app->db;
        $connection->createCommand($sql)->execute();
        //return 1;
    }
    public function actionTestData()
    {
        $filePatch = Yii::getAlias("@console/sql/Step3_TestData.sql");
        $sql = file_get_contents($filePatch);
        $connection = \Yii::$app->db;
        $connection->createCommand($sql)->execute();
        //return 1;
    }


    public function actionDemoData()
    {

    }

    public function actionProdData()
    {

    }

    public function actionDevData()
    {

    }

    public function actionClear()
    {
        $path = [
            'backend assets' => '@backend/web/assets',
            'frontend assets' => '@frontend/web/assets'
        ];
        foreach ($path as $k => $folder) {
            $_folder = Yii::getAlias($folder);
            $fileSize = delFile($_folder);
            $size = getArrSum($fileSize);
            printf($k . ':%0.3fM', $size / (1024 * 1024));
            echo "\n";
        }
    }

    public function actionClearAsset()
    {
        $path = [
            'backend assets' => '@backend/web/assets',
            'frontend assets' => '@frontend/web/assets'
        ];
        foreach ($path as $k => $folder) {
            $_folder = Yii::getAlias($folder);
            $fileSize = delFile($_folder);
            $size = getArrSum($fileSize);
            printf($k . ':%0.3fM', $size / (1024 * 1024));
            echo "\n";
        }
    }

    public function actionAbc($id)
    {
        \common\corelib\order\RefundLib::autoAgreeReturn($id);
    }

}


////public function
function delFile($path)
{
    $fileSize = array();
    if (is_dir($path)) {
        if ($dh = opendir($path)) {
            while (($file = readdir($dh)) !== false) {
                if ($file != '.' && $file != '..' && $file != '.gitignore') {
                    $fileSize[] = delFile($path . '/' . $file);
                }
            }
            closedir($dh);
        }
        //for cal size
        $fileSize['file'] = 0;
        @rmdir($path);
    } else {
        if (is_file($path)) {
            $fileSize[] = $fsize = filesize($path);
            @unlink($path);
        }
    }
    return $fileSize;
}

/**
 * @param $arr
 * @return number
 */
function getArrSum(&$arr)
{
    if (is_array($arr)) {
        foreach ($arr as &$value) {
            $value = getArrSum($value);
        }
        return array_sum($arr);
    } else {
        return $arr;
    }
}
