<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%relationships}}".
 *
 * @property integer $cid
 * @property integer $mid
 */
class Relationships extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%relationships}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cid', 'mid'], 'integer'],
            [['cid', 'mid'], 'unique', 'targetAttribute' => ['cid', 'mid'], 'message' => 'The combination of 内容主键 and 项目主键 has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cid' => Yii::t('app', '内容主键'),
            'mid' => Yii::t('app', '项目主键'),
        ];
    }

    /**
     * @inheritdoc
     * @return RelationshipsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RelationshipsQuery(get_called_class());
    }
}
