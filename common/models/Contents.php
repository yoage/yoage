<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%contents}}".
 *
 * @property integer $cid
 * @property string $title
 * @property string $slug
 * @property integer $created
 * @property integer $modified
 * @property string $text
 * @property integer $order
 * @property string $author_id
 * @property string $template
 * @property string $type
 * @property string $status
 * @property string $password
 * @property integer $comments_num
 * @property string $allow_comment
 * @property string $allow_ping
 * @property string $allow_feed
 */
class Contents extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contents}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created', 'modified', 'order', 'comments_num'], 'integer'],
            [['text'], 'string'],
            [['title', 'slug', 'author_id'], 'string', 'max' => 200],
            [['template', 'password'], 'string', 'max' => 32],
            [['type', 'status'], 'string', 'max' => 16],
            [['allow_comment', 'allow_ping', 'allow_feed'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cid' => Yii::t('app', 'post表主键'),
            'title' => Yii::t('app', '内容标题'),
            'slug' => Yii::t('app', '内容缩略名'),
            'created' => Yii::t('app', '内容生成时的GMT unix时间戳'),
            'modified' => Yii::t('app', '内容更改时的GMT unix时间戳'),
            'text' => Yii::t('app', '内容文字'),
            'order' => Yii::t('app', '排序'),
            'author_id' => Yii::t('app', '内容所属用户id'),
            'template' => Yii::t('app', '内容使用的模板'),
            'type' => Yii::t('app', '内容类别'),
            'status' => Yii::t('app', '内容状态'),
            'password' => Yii::t('app', '受保护内容,此字段对应内容保护密码'),
            'comments_num' => Yii::t('app', '内容所属评论数,冗余字段'),
            'allow_comment' => Yii::t('app', '是否允许评论'),
            'allow_ping' => Yii::t('app', '是否允许ping'),
            'allow_feed' => Yii::t('app', '允许出现在聚合中'),
        ];
    }

    /**
     * @inheritdoc
     * @return ContentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ContentsQuery(get_called_class());
    }
}
