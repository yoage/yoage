<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%fields}}".
 *
 * @property string $cid
 * @property string $name
 * @property string $type
 * @property string $str_value
 * @property integer $int_value
 * @property double $float_value
 */
class Fields extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%fields}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cid', 'name'], 'required'],
            [['cid', 'int_value'], 'integer'],
            [['str_value'], 'string'],
            [['float_value'], 'number'],
            [['name'], 'string', 'max' => 200],
            [['type'], 'string', 'max' => 8]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cid' => Yii::t('app', 'Cid'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'str_value' => Yii::t('app', 'Str Value'),
            'int_value' => Yii::t('app', 'Int Value'),
            'float_value' => Yii::t('app', 'Float Value'),
        ];
    }

    /**
     * @inheritdoc
     * @return FieldsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FieldsQuery(get_called_class());
    }
}
