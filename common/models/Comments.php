<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%comments}}".
 *
 * @property integer $coid
 * @property integer $cid
 * @property integer $created
 * @property string $author
 * @property integer $author_id
 * @property integer $owner_id
 * @property string $mail
 * @property string $url
 * @property string $ip
 * @property string $agent
 * @property string $text
 * @property string $type
 * @property string $status
 * @property integer $parent
 */
class Comments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%comments}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cid', 'created', 'author_id', 'owner_id', 'parent'], 'integer'],
            [['text'], 'string'],
            [['author', 'mail', 'url', 'agent'], 'string', 'max' => 200],
            [['ip'], 'string', 'max' => 64],
            [['type', 'status'], 'string', 'max' => 16]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'coid' => Yii::t('app', 'comment表主键'),
            'cid' => Yii::t('app', 'post表主键,关联字段'),
            'created' => Yii::t('app', '评论生成时的GMT unix时间戳'),
            'author' => Yii::t('app', '评论作者'),
            'author_id' => Yii::t('app', '评论所属用户id'),
            'owner_id' => Yii::t('app', '评论所属内容作者id'),
            'mail' => Yii::t('app', '评论者邮件'),
            'url' => Yii::t('app', '评论者网址'),
            'ip' => Yii::t('app', '评论者ip地址'),
            'agent' => Yii::t('app', '评论者客户端'),
            'text' => Yii::t('app', '评论文字'),
            'type' => Yii::t('app', '评论类型'),
            'status' => Yii::t('app', '评论状态'),
            'parent' => Yii::t('app', '父级评论'),
        ];
    }

    /**
     * @inheritdoc
     * @return CommentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CommentsQuery(get_called_class());
    }
}
